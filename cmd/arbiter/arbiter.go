package arbiter

import (
	"log"
	"os"
	"time"

	"mr-cache/internal/pkg/arbiter"
	"mr-cache/pkg/acl"
	"mr-cache/pkg/cache"
	"mr-cache/pkg/cache/config"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

const (
	DefaultAclFilename = "acl.json"
)

var (
	addr              *string
	aclFilename       *string
	exportCacheConfig func() *cache.Config
	enableAuth        *bool
	nodes             *[]string
	port              *int
	readTimeout       *time.Duration
	writeTimeout      *time.Duration
)

func SetFlags(fs *pflag.FlagSet) {
	addr = fs.String("server.addr", arbiter.DefaultAddr, "server bind addr")
	aclFilename = fs.String("acl_filename", DefaultAclFilename, "acl filename")
	exportCacheConfig = config.Export(fs, "")
	enableAuth = fs.Bool("server.enable_auth", false, "enable auth")
	nodes = fs.StringSlice("nodes", nil, "nodes urls")
	port = fs.Int("server.port", arbiter.DefaultPort, "server bind port")
	readTimeout = fs.Duration("server.read_timeout", arbiter.DefaultReadTimeout, " server read timeout")
	writeTimeout = fs.Duration("server.write_timeout", arbiter.DefaultReadTimeout, " server write timeout")
}

func Use() string {
	return "arbiter"
}

func Long() string {
	return "in memory cache arbiter aka redis"
}

func Short() string {
	return "Cache arbiter server"
}

func Execute(cmd *cobra.Command, args []string) {
	var a *acl.ACL
	if *enableAuth {
		file, err := os.Open(*aclFilename)
		if err != nil {
			log.Fatalf("acl file open error: %v", err)
		}

		a = acl.New()

		err = a.Load(file)
		if err != nil {
			log.Fatalf("acl load error: %v", err)
		}
	}

	conf := &arbiter.Config{
		Addr:         *addr,
		ACL:          a,
		CacheConfig:  exportCacheConfig(),
		EnableAuth:   *enableAuth,
		Nodes:        *nodes,
		Port:         *port,
		ReadTimeout:  *readTimeout,
		WriteTimeout: *writeTimeout,
	}

	n := arbiter.New(conf)

	err := n.Run()
	if err != nil {
		log.Fatalf("node run error: %v", err)
	}
}
