package swag

import (
	"log"

	_ "mr-cache/docs"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func Use() string {
	return "swag"
}

func Long() string {
	return "in memory cache server aka redis api documentation"
}

func Short() string {
	return "Cache node api documentation server"
}

func Execute(cmd *cobra.Command, args []string) {
	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	err := r.Run(":8080")
	if err != nil {
		log.Fatalf("server run error: %v", err)
	}
}
