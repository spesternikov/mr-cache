package client

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"

	"mr-cache/internal/pkg/client"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

var (
	url      *string
	password *string
	timeout  *time.Duration
	user     *string
)

func SetFlags(fs *pflag.FlagSet) {
	url = fs.String("url", "", "cache url")
	password = fs.String("password", "", "password")
	timeout = fs.Duration("timeout", client.DefaultTimeout, "http client timeout")
	user = fs.String("user", "", "user")
}

func Use() string {
	return "client"
}

func Long() string {
	return "client in memory cache server aka redis"
}

func Short() string {
	return "Cache client"
}

func Execute(cmd *cobra.Command, args []string) {
	conf := &client.Config{
		Url:      *url,
		Password: *password,
		Timeout:  *timeout,
		User:     *user,
	}

	c := client.New(conf)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		for {
			fmt.Println("Please enter command")

			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				s := scanner.Text()
				err := c.Do(ctx, s)
				if err != nil {
					fmt.Printf("execute comand error: %v\n", err)
				}
			}
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	cancel()
}
