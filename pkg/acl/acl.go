package acl

import (
	"encoding/json"
	"fmt"
	"io"
)

type ACL struct {
	users []user
}

type user struct {
	User     string `json:"user"`
	Password string `json:"password"`
}

func New() *ACL {
	return &ACL{}
}

func (a *ACL) Load(r io.Reader) error {
	var users []user

	err := json.NewDecoder(r).Decode(&users)
	if err != nil {
		return fmt.Errorf("json decode users error: %v", err)
	}

	a.users = users

	return nil
}

func (a *ACL) GetUsers() map[string]string {
	users := make(map[string]string)

	for _, user := range a.users {
		users[user.User] = user.Password
	}

	return users
}

func (a *ACL) Check(name, password string) bool {
	for _, user := range a.users {
		if user.User == name && user.Password == password {
			return true
		}
	}

	return false
}
