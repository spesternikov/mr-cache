package cache

import (
	"runtime"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGet(t *testing.T) {
	assert := assert.New(t)

	c := New(nil)
	defer c.Destroy()

	c.Set("key", "value", DefaultExpiration)

	val, found, err := c.Get("key")

	assert.Nil(err)

	assert.True(found, "key not found")

	assert.Equal(val, "value", "values not equals")
}

func TestHGet(t *testing.T) {
	assert := assert.New(t)

	c := New(nil)
	defer c.Destroy()

	value := map[string]string{
		"hkey": "value",
	}
	c.Set("key", value, DefaultExpiration)

	val, found, err := c.HGet("key", "hkey")

	assert.Nil(err)

	assert.True(found, "key not found")

	assert.Equal(val, "value", " values not equals")
}

func TestLGet(t *testing.T) {
	assert := assert.New(t)

	c := New(nil)
	defer c.Destroy()

	value := []string{"value1", "value2"}
	c.Set("key", value, DefaultExpiration)

	val, found, err := c.LGet("key", 0)

	assert.Nil(err)

	assert.True(found, "key not found")

	assert.Equal(val, "value1", "values not equals")
}

func TestSetDelete(t *testing.T) {
	assert := assert.New(t)

	c := New(nil)
	defer c.Destroy()

	c.Set("key", "value", DefaultExpiration)

	found := c.Del("key")

	assert.True(found, "not found")

	val, found, err := c.Get("key")

	assert.Nil(err)

	assert.False(found, "unexpected found")

	assert.Equal(val, "", "values not equals")
}

func TestKeys(t *testing.T) {
	assert := assert.New(t)

	c := New(nil)
	defer c.Destroy()

	c.Set("key1", "value", DefaultExpiration)

	hash := map[string]string{
		"hkey": "value",
	}
	c.Set("key2", hash, DefaultExpiration)

	list := []string{"value1", "value2"}
	c.Set("key3", list, DefaultExpiration)

	keys := c.Keys()

	assert.NotEmpty(keys, "keys is empty")

	assert.Equal(keys, []string{"key1", "key2", "key3"}, "keys not equal")
}

func BenchmarkCacheGet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	c.Set("key", "value", DefaultExpiration)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Get("key")
	}
}

func BenchmarkCacheHGet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := map[string]string{
		"hkey": "value",
	}
	c.Set("key", value, DefaultExpiration)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.HGet("key", "hkey")
	}
}

func BenchmarkCacheLGet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := []string{"value1", "value2"}
	c.Set("key", value, DefaultExpiration)

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.LGet("key", 0)
	}
}

func BenchmarkCacheGetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	c.Set("key", "value", DefaultExpiration)

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.LGet("key", 0)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheHGetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := map[string]string{
		"hkey": "value",
	}
	c.Set("key", value, DefaultExpiration)

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.HGet("key", "hkey")
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheLGetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := []string{"value1", "value2"}
	c.Set("key", value, DefaultExpiration)

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.LGet("key", 0)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheSet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", "value", DefaultExpiration)
	}
}

func BenchmarkCacheHSet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := map[string]string{
		"hkey": "value",
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", value, DefaultExpiration)
	}
}

func BenchmarkCacheLSet(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := []string{"value1", "value2"}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", value, DefaultExpiration)
	}
}

func BenchmarkCacheSetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", "value", DefaultExpiration)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheHSetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	value := map[string]string{
		"hkey": "value",
	}

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", value, DefaultExpiration)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheLSetParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	value := []string{"value1", "value2"}

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", value, DefaultExpiration)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheSetDelete(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", "value", DefaultExpiration)
		c.Del("key")
	}
}

func BenchmarkCacheHSetDelete(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := map[string]string{
		"hkey": "value",
	}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", value, DefaultExpiration)
		c.Del("key")
	}
}

func BenchmarkCacheLSetDelete(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	value := []string{"value1", "value2"}

	b.StartTimer()
	for n := 0; n < b.N; n++ {
		c.Set("key", value, DefaultExpiration)
		c.Del("key")
	}
}

func BenchmarkCacheSetDeleteParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", "value", DefaultExpiration)
				c.Del("key")
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheHSetDeleteParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	value := map[string]string{
		"hkey": "value",
	}

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", value, DefaultExpiration)
				c.Del("key")
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func BenchmarkCacheLSetDeleteParallels(b *testing.B) {
	b.StopTimer()

	c := New(nil)
	defer c.Destroy()

	wg := new(sync.WaitGroup)
	workers := runtime.NumCPU()
	num := b.N / workers
	wg.Add(workers)

	value := []string{"value1", "value2"}

	b.StartTimer()
	for i := 0; i < workers; i++ {
		go func() {
			for j := 0; j < num; j++ {
				c.Set("key", value, DefaultExpiration)
				c.Del("key")
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
