package config

import (
	"time"

	"mr-cache/pkg/cache"
)

type Config interface {
	Bool(name string, defValue bool, desc string) *bool
	Duration(name string, defValue time.Duration, desc string) *time.Duration
	String(name string, defValue string, desc string) *string
}

func Export(config Config, prefix string) func() *cache.Config {
	prefix = sanitize(prefix)
	var (
		backupFilename    = config.String(prefix+"cache.backup_filename", cache.DefaultBackupFilename, "cache backup filename")
		backupInterval    = config.Duration(prefix+"cache.backup_interval", cache.DefaultCleanupInterval, "cache backup interval")
		cleanupInterval   = config.Duration(prefix+"cache.cleanup_interval", cache.DefaultCleanupInterval, "cache cleanup interval")
		defaultExpiration = config.Duration(prefix+"cache.default_expiration", cache.DefaultExpiration, "cache default expiration")
		enableBackup      = config.Bool(prefix+"cache.enable_backup", true, "cache enable backup")
	)

	return func() *cache.Config {
		return &cache.Config{
			BackupFilename:    *backupFilename,
			BackupInterval:    *backupInterval,
			CleanupInterval:   *cleanupInterval,
			DefaultExpiration: *defaultExpiration,
			EnableBackup:      *enableBackup,
		}
	}
}

func sanitize(p string) string {
	if n := len(p); n != 0 {
		if p[n-1] != '.' {
			return p + "."
		}
	}
	return p
}
