package cache

import (
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"sync"
	"time"
)

const (
	DefaultBackupFilename  = "cache.backup"
	DefaultBackupInterval  = 5 * time.Minute
	DefaultCleanupInterval = 5 * time.Minute
	DefaultExpiration      = 5 * time.Minute
)

var (
	ErrBadRequest    = errors.New("bad request")
	ErrIndexOutRange = errors.New("index out of range")
	ErrItemNotFound  = errors.New("item not found")
)

type Config struct {
	BackupFilename    string
	BackupInterval    time.Duration
	CleanupInterval   time.Duration
	DefaultExpiration time.Duration
	EnableBackup      bool
}

type Cache struct {
	backup   *backup
	cleaner  *cleaner
	config   Config
	elements map[string]Element
	guard    sync.RWMutex
	once     sync.Once
	stop     chan struct{}
	wg       sync.WaitGroup
}

type Element struct {
	Expiration int64
	Val        interface{}
}

type backup struct {
	Filename string
	Interval time.Duration

	cache *Cache
	stop  chan struct{}
}

type cleaner struct {
	Interval time.Duration

	cache *Cache
	stop  chan struct{}
}

func init() {
	gob.Register(map[string]string{})
}

func New(c *Config) *Cache {
	cache := new(Cache)

	cache.config = c.withDefaults()

	cache.elements = make(map[string]Element)
	cache.stop = make(chan struct{})

	if cache.config.EnableBackup {
		cache.backup = newBackup(cache, cache.config.BackupFilename, cache.config.BackupInterval, cache.stop)

		cache.wg.Add(1)
		go func() {
			cache.backup.Run()
			cache.wg.Done()
		}()
	}

	cache.cleaner = newCleaner(cache, cache.config.CleanupInterval, cache.stop)

	cache.wg.Add(1)
	go func() {
		cache.cleaner.Run()
		cache.wg.Done()
	}()

	return cache
}

func newBackup(cache *Cache, filename string, interval time.Duration, stop chan struct{}) *backup {
	return &backup{
		Filename: filename,
		Interval: interval,
		cache:    cache,
		stop:     stop,
	}
}

func newCleaner(cache *Cache, interval time.Duration, stop chan struct{}) *cleaner {
	return &cleaner{
		Interval: interval,
		cache:    cache,
		stop:     stop,
	}
}

func (c *Config) withDefaults() (config Config) {
	if c != nil {
		config = *c
	}

	if config.BackupFilename == "" {
		config.BackupFilename = DefaultBackupFilename
	}

	if config.BackupInterval == 0 {
		config.BackupInterval = DefaultBackupInterval
	}

	if config.CleanupInterval == 0 {
		config.CleanupInterval = DefaultCleanupInterval
	}

	if config.DefaultExpiration == 0 {
		config.DefaultExpiration = DefaultExpiration
	}

	return config
}

func (c *Cache) get(key string) (interface{}, bool) {
	element, ok := c.elements[key]
	if !ok {
		return nil, false
	}

	if element.Expired() {
		return nil, false
	}

	return element.Val, true
}

func (c *Cache) load(r io.Reader) error {
	var elements map[string]Element

	err := gob.NewDecoder(r).Decode(&elements)
	if err != nil {
		return fmt.Errorf("json decode elements error: %v", err)
	}

	c.guard.RLock()
	defer c.guard.RUnlock()

	for k, v := range elements {
		ov, found := c.elements[k]
		if !found || ov.Expired() {
			c.elements[k] = v
		}
	}

	return nil
}

func (c *Cache) save(w io.Writer) error {
	c.guard.RLock()

	m := make(map[string]Element, len(c.elements))
	now := time.Now().UnixNano()
	for k, v := range c.elements {
		if v.Expiration > 0 {
			if now > v.Expiration {
				continue
			}
		}
		m[k] = v
	}

	c.guard.RUnlock()

	err := gob.NewEncoder(w).Encode(m)
	if err != nil {
		return fmt.Errorf("json encode elements error: %v", err)
	}

	return nil
}

func (c *Cache) CleanExpired() {
	now := time.Now().UnixNano()

	c.guard.Lock()

	for k, v := range c.elements {
		if v.Expiration > 0 && now > v.Expiration {
			delete(c.elements, k)
		}
	}

	c.guard.Unlock()
}

func (c *Cache) Contains(key string) bool {
	_, found := c.get(key)

	return found
}

func (c *Cache) Del(key string) bool {
	c.guard.Lock()

	_, ok := c.elements[key]
	if ok {
		delete(c.elements, key)
	}

	c.guard.Unlock()
	return ok
}

func (c *Cache) Destroy() {
	c.once.Do(func() {
		close(c.stop)
		c.wg.Wait()
	})
}

func (c *Cache) Get(key string) (string, bool, error) {
	c.guard.RLock()

	val, ok := c.get(key)

	c.guard.RUnlock()

	if !ok {
		return "", false, nil
	}

	res, ok := val.(string)
	if !ok {
		return "", false, ErrBadRequest
	}

	return res, ok, nil
}

func (c *Cache) HGet(key string, hKey string) (string, bool, error) {
	c.guard.RLock()

	val, ok := c.get(key)

	c.guard.RUnlock()

	if !ok {
		return "", false, nil
	}

	m, ok := val.(map[string]string)
	if !ok {
		return "", false, ErrBadRequest
	}

	res, ok := m[hKey]
	if !ok {
		return "", true, ErrItemNotFound
	}

	return res, true, nil
}

func (c *Cache) Keys() []string {
	c.guard.RLock()

	keys := make([]string, 0, len(c.elements))
	for key := range c.elements {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	c.guard.RUnlock()

	return keys
}

func (c *Cache) LGet(key string, index int) (string, bool, error) {
	c.guard.RLock()

	val, ok := c.get(key)

	c.guard.RUnlock()

	if !ok {
		return "", false, nil
	}

	list, ok := val.([]string)
	if !ok {
		return "", false, ErrBadRequest
	}

	if len(list) != 0 && index >= 0 && index < len(list) {
		return list[index], true, nil
	} else {
		return "", false, ErrIndexOutRange
	}
}

func (c *Cache) Set(key string, value interface{}, d time.Duration) {
	var e int64
	if d == 0 {
		d = c.config.DefaultExpiration
	}
	if d > 0 {
		e = time.Now().Add(d).UnixNano()
	}

	c.guard.Lock()
	c.elements[key] = Element{
		Expiration: e,
		Val:        value,
	}
	c.guard.Unlock()
}

func (b *backup) init() error {
	var file *os.File

	if stat, err := os.Stat(b.Filename); os.IsNotExist(err) {
		file, err = os.Create(b.Filename)
		if err != nil {
			return fmt.Errorf("create backup file %s error: %v", b.Filename, err)
		}
		defer file.Close()
	} else if err == nil {
		file, err = os.Open(b.Filename)
		if err != nil {
			return fmt.Errorf("open backup file %s error: %v", b.Filename, err)
		}
		defer file.Close()

		if stat.Size() != 0 {
			err := b.cache.load(file)
			if err != nil {
				return fmt.Errorf("load cache error: %v", err)
			}
		}
	}

	return nil
}

func (b *backup) save() error {
	file, err := os.Create(b.Filename)
	if err != nil {
		return fmt.Errorf("open backup file error: %v", err)
	}
	err = b.cache.save(file)
	if err != nil {
		return fmt.Errorf("cache save error: %v", err)
	}

	return nil
}

func (b *backup) Run() {
	err := b.init()
	if err != nil {
		log.Fatalf("backup init error: %v", err)
	}

	for {
		select {
		case <-time.After(b.Interval):
			err := b.save()
			if err != nil {
				log.Fatalf("%v", err)
			}
		case <-b.stop:
			err := b.save()
			if err != nil {
				log.Fatalf("%v", err)
			}
			return
		}
	}
}

func (c *cleaner) Run() {
	for {
		select {
		case <-time.After(c.Interval):
			c.cache.CleanExpired()
		case <-c.stop:
			return
		}
	}
}

func (e Element) Expired() bool {
	return time.Now().UnixNano() > e.Expiration
}
