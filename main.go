// @title MR-Cache API
// @version 1.0
// @description Cache server aka redis.

// @contact.name Stepan Pesternikov
// @contact.email spesternikov@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @securityDefinitions.basic BasicAuth

package main

import (
	"log"

	"mr-cache/cmd/arbiter"
	"mr-cache/cmd/client"
	"mr-cache/cmd/node"
	"mr-cache/cmd/swag"

	"github.com/spf13/cobra"
)

func main() {
	rootCmd := &cobra.Command{Use: "mr-cache"}

	a := &cobra.Command{
		Use:   arbiter.Use(),
		Short: arbiter.Short(),
		Long:  arbiter.Long(),
		Run:   arbiter.Execute,
	}

	c := &cobra.Command{
		Use:   client.Use(),
		Short: client.Short(),
		Long:  client.Long(),
		Run:   client.Execute,
	}

	n := &cobra.Command{
		Use:   node.Use(),
		Short: node.Short(),
		Long:  node.Long(),
		Run:   node.Execute,
	}

	s := &cobra.Command{
		Use:   swag.Use(),
		Short: swag.Short(),
		Long:  swag.Long(),
		Run:   swag.Execute,
	}

	arbiter.SetFlags(a.Flags())
	client.SetFlags(c.Flags())
	node.SetFlags(n.Flags())

	rootCmd.AddCommand(a)
	rootCmd.AddCommand(c)
	rootCmd.AddCommand(n)
	rootCmd.AddCommand(s)

	err := rootCmd.Execute()
	if err != nil {
		log.Fatalf("execute error: %v", err)
	}
}
