# MR-CACHE

Тестовое задание на создание In-Memory Cache aka Redis

## Описание

Реализован, как CLI-приложение.
Поддерживает 4 режима работы:

* node - http кэш сервер
* arbiter - арбитр кэш серверов (для масштабирования)
* client - консольный клиент для работы с mr-cache
* swag - сервер REST API-документации

### Кэш

Кэш поддерживет следующие команды:

* get - получение значения строки по ключу
* set - установка значения строки по ключу
* hget - получение значения строки из хэша по ключу
* hset - установка значения хэша по ключу
* lget - получение значения строки из списка строк по индексу
* del - удаление значния и по ключу
* keys - получение списка ключей

### Тесты

Для запуска тестов пакета `pkg/cache` необходимо выполнить
следующую команду:

```bash
    $ cd pkg/cache
    $ go test
```

### Нагрузочные тесты

Для запуска нагрузочных тестов пакета `pkg/cache` необходимо 
выполнить следующую команду:

```bash
    $ cd pkg/cache
    $ go test -bench=. -benchmem
```

## Менеджер зависимостей

В данном проекте использован менеджер зависимостей go.mod.

## Сборка

Для выполнения сборки необходимо выполнить следующую команду:

```bash
    $ go build -o mr-cache
```

## Справка

Для получения справки необходимо выполнить следующую команду:

```bash
    $ mr-cache -h [--help]
```

## Авторизация

В данной реализации используется BasicAuth. В директории `etc` 
находится файл `acl.json.example`, в котором описываются 
пользователи и пароли:

```
[
  {
    "user": "ivan", // Пользователь
    "password": "12345678" // Пароль
  }
  
  ...
]
```

## Режимы работы

### Кэш сервер

HTTP кэш сервер.

#### Запуск

Для зпуска необходимо выполнить следующую команду:

```bash
    $ mr-cache node [flags]
```

#### Справка

Для получения справки необходимо выполнить следующую команду:

```bash
    $ mr-cache node -h [--help]
```

#### Параметры (flags)

* **acl_filename** - файл acl
(string, по умолчанию: `acl.json`)

* **cache.backup_filename** - backup файл кэша
(string, по умолчанию: `cache.backup`)

* **cache.cleanup_interval** - интервал очистки кэша
(duration, по умолчанию: `5m`)

* **cache.default_expiration** - время вытеснения ключа 
по умолчанию (duration, по умолчанию: `5m`)

* **cache.enable_backup** - включение режима с сохранением
кэша на диск (bool, по умолчанию: `true`)

* **server.enable_auth** - включение авторизацию
(bool, по умолчанию: `false`)

* **server.addr** - адрес сервера
(string, по умолчанию: `127.0.0.1`)

* **server.port** - порт сервера
(int, по умолчанию: `1234`)

* **server.read_timeout** - read timeout сервера
(duration, по умолчанию: `1m`)

* **server.write_timeout** - write timeout сервера
(duration, по умолчанию: `1m`)

#### Пример запуска

```bash
    $ mr-cache node --server.addr="127.0.0.1" --server.port=1234
```

### Арбитр кэш серверов

Арбитр выступает в роли реверс-прокси сервера к кэш серверам.
Также арбитер хранит ключи, которые появляются на кэш серверах.

#### Запуск

Для зпуска необходимо выполнить следующую команду:

```bash
    $ mr-cache arbiter [flags]
```

#### Справка

Для получения справки необходимо выполнить следующую команду:

```bash
    $ mr-cache arbiter -h [--help]
```

#### Параметры (flags)

* **acl_filename** - файл acl
(string, по умолчанию: `acl.json`)

* **cache.backup_filename** - backup файл кэша
(string, по умолчанию: `cache.backup`)

* **cache.cleanup_interval** - интервал очистки кэша
(duration, по умолчанию: `5m`)

* **cache.default_expiration** - время вытеснения ключа 
по умолчанию (duration, по умолчанию: `5m`)

* **cache.enable_backup** - включение режима с сохранением
кэша на диск (bool, по умолчанию: `true`)

* **nodes** - список url-адресов кэш серверов (stringSlice)

* **server.enable_auth** - включение авторизацию
(bool, по умолчанию: `false`)

* **server.addr** - адрес сервера
(string, по умолчанию: `127.0.0.1`)

* **server.port** - порт сервера
(int, по умолчанию: `1234`)

* **server.read_timeout** - read timeout сервера
(duration, по умолчанию: `1m`)

* **server.write_timeout** - write timeout сервера
(duration, по умолчанию: `1m`)

#### Пример запуска кластера

Запустить первый кэш сервер:

```bash
    $ mr-cache node --server.addr="127.0.0.1" --server.port=1234
```

Запустить второй кэш сервер:

```bash
    $ mr-cache node --server.addr="127.0.0.1"--server.port=1235
```

Запустить арбитр:

```bash
    $ mr-cache arbiter --server.addr="127.0.0.1" --server.port=1233 --nodes="http://127.0.0.1:1234" --nodes="http://127.0.0.1:1235"
```

### Консольный клиент для работы с mr-cache

Консольный клиент для работы с кэшом.

#### Запуск

Для зпуска необходимо выполнить следующую команду:

```bash
    $ mr-cache client [flags]
```

#### Справка

Для получения справки необходимо выполнить следующую команду:

```bash
    $ mr-cache client -h [--help]
```

#### Параметры (flags)

* **password** - пароль (string)

* **timeout** - таймаут http клиента
(duration, по умолчанию: `1m`)

* **url** - url-адрес кэш сервера (string)

* **user** - пользователь (string)

#### Пример запуска клиента

```bash
    $ /mr-cache client --url="http://127.0.0.1:1235" --user=ivan --password=12345678
```

#### Команды клиента

Клиент парсит входную комнду, как shell-строку. Если в строке
имеются пробелы или спецсимволы, то их нужно экранировать 
используя `\`.
[expiration] - необязательный параметр, если не указан,
то будет принято значение по умолчанию на кэш сервере.

##### get

Получение значения строки по ключу

```bash
    get key
```

* key - string

Пример:

```bash
    get foo
    get "foo bar"
```

##### set

Установка значения строки по ключу

```bash
    set key value [expiration]
```

* key - string
* value - string

Пример:

```bash
    set foo bar
    set foo "bar foo"
    set foo bar 5m
    set foo "bar foo" 5m
```

##### hget

Получение значения строки из хэша по ключу

```bash
    hget key hkey
```

* key - string
* hkey - string

Пример:

```bash
    hget foo bar
    hget foo "bar foo"
```

##### hset

Установка значения хэша по ключу

```bash
    hset key value [expiration]
```

* key - string
* value - json object

Пример:

```bash
    hset foo "{\"bar\": \"foo\"}"
    hset foo "{\"bar\": \"foo\"}" 5m
```

##### lget

Получение значения строки из списка строк по индексу

```bash
    lget key index
```

* key - string
* index - int

Пример:

```bash
    lget foo 123
```

##### lset

```bash
    hset key value [expiration]
```

* key - string
* value - json string array

Пример:

```bash
    hset foo "[\"bar\", \"foo\"]"
    hset foo "[\"bar\", \"foo\"]" 5m
```

##### del

Удаление значния и по ключу

```bash
    del key
```

* key - string

##### keys

Получение списка ключей

```bash
    keys
```

##### help

Вывод справки

```bash
    help
```

### Сервер REST API-документации

Запускает сервер с REST API-документацией на 8080 порту. 

[Перейти к документации.](http://127.0.0.1:8080)

#### Запуск

Для зпуска необходимо выполнить следующую команду:

```bash
    $ mr-cache swag
```