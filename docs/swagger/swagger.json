{
    "swagger": "2.0",
    "info": {
        "description": "Cache server aka redis.",
        "title": "MR-Cache API",
        "contact": {
            "name": "Stepan Pesternikov",
            "email": "spesternikov@gmail.com"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "version": "1.0"
    },
    "host": "{{.Host}}",
    "basePath": "{{.BasePath}}",
    "paths": {
        "/del": {
            "post": {
                "description": "Delete item from cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "del",
                "parameters": [
                    {
                        "description": "del dto",
                        "name": "del",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Del"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Del"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/get": {
            "post": {
                "description": "Get string from cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "get",
                "parameters": [
                    {
                        "description": "get dto",
                        "name": "value",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Value"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Value"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/hget": {
            "post": {
                "description": "Get string-hash item from cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "hget",
                "parameters": [
                    {
                        "description": "hget dto",
                        "name": "map",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Map"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Value"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "403": {
                        "description": "Forbidden",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/hset": {
            "post": {
                "description": "Set hash to cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "hset",
                "parameters": [
                    {
                        "description": "hset dto",
                        "name": "map",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Map"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Base"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/keys": {
            "get": {
                "description": "Get cache keys",
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "keys",
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Values"
                        }
                    }
                }
            }
        },
        "/lget": {
            "post": {
                "description": "Get string-list item from cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "lget",
                "parameters": [
                    {
                        "description": "lget dto",
                        "name": "list",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.List"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Value"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "403": {
                        "description": "Forbidden",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/lset": {
            "post": {
                "description": "Set list to cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "lset",
                "parameters": [
                    {
                        "description": "lset dto",
                        "name": "list",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.List"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Base"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        },
        "/set": {
            "post": {
                "description": "Set string to cache by key",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "cache"
                ],
                "summary": "set",
                "parameters": [
                    {
                        "description": "set dto",
                        "name": "value",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Value"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Base"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/dto.Error"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "dto.Base": {
            "type": "object",
            "properties": {
                "expiration": {
                    "type": "string",
                    "example": "1m"
                },
                "key": {
                    "type": "string",
                    "example": "key"
                }
            }
        },
        "dto.Del": {
            "type": "object",
            "properties": {
                "key": {
                    "type": "string",
                    "example": "key"
                }
            }
        },
        "dto.Error": {
            "type": "object",
            "properties": {
                "message": {
                    "type": "string",
                    "example": "message"
                }
            }
        },
        "dto.List": {
            "type": "object",
            "properties": {
                "expiration": {
                    "type": "string"
                },
                "index": {
                    "type": "integer",
                    "example": 1
                },
                "key": {
                    "type": "string",
                    "example": "key"
                },
                "value": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    },
                    "example": [
                        "value1",
                        "value2"
                    ]
                }
            }
        },
        "dto.Map": {
            "type": "object",
            "properties": {
                "expiration": {
                    "type": "string"
                },
                "hKey": {
                    "type": "string",
                    "example": "key"
                },
                "key": {
                    "type": "string",
                    "example": "key"
                },
                "value": {
                    "type": "object",
                    "example": {"key":"value"}
                }
            }
        },
        "dto.Value": {
            "type": "object",
            "properties": {
                "expiration": {
                    "type": "string"
                },
                "key": {
                    "type": "string",
                    "example": "key"
                },
                "value": {
                    "type": "string",
                    "example": "value"
                }
            }
        },
        "dto.Values": {
            "type": "object",
            "properties": {
                "values": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    },
                    "example": [
                        "key1",
                        "key2"
                    ]
                }
            }
        }
    },
    "securityDefinitions": {
        "BasicAuth": {
            "type": "basic"
        }
    }
}