package arbiter

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"

	"mr-cache/internal/pkg/dto"
	"mr-cache/pkg/acl"
	"mr-cache/pkg/cache"
)

const (
	DefaultAddr         = "127.0.0.1"
	DefaultPort         = 1233
	DefaultReadTimeout  = time.Minute
	DefaultWriteTimeout = time.Minute
)

type Config struct {
	Addr         string
	ACL          *acl.ACL
	CacheConfig  *cache.Config
	EnableAuth   bool
	Nodes        []string
	Port         int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

type Arbiter struct {
	config   Config
	cache    *cache.Cache
	getPaths map[string]string
	proxies  []*httputil.ReverseProxy
	server   *http.Server
	setPaths map[string]string
	urls     []*url.URL
}

func (c *Config) withDefaults() (config Config) {
	if c != nil {
		config = *c
	}

	if config.Addr == "" {
		config.Addr = DefaultAddr
	}

	if config.Port == 0 {
		config.Port = DefaultPort
	}

	if config.ReadTimeout == 0 {
		config.ReadTimeout = DefaultReadTimeout
	}

	if config.WriteTimeout == 0 {
		config.WriteTimeout = DefaultWriteTimeout
	}

	return config
}

func New(c *Config) *Arbiter {
	a := new(Arbiter)

	a.config = c.withDefaults()

	a.server = &http.Server{
		Addr:         a.config.Addr + ":" + strconv.Itoa(a.config.Port),
		Handler:      http.HandlerFunc(a.handleRequest),
		ReadTimeout:  a.config.ReadTimeout,
		WriteTimeout: a.config.WriteTimeout,
	}

	a.getPaths = map[string]string{
		"/get":  "POST",
		"/hget": "POST",
		"/lget": "POST",
	}

	a.setPaths = map[string]string{
		"/set":  "POST",
		"/hset": "POST",
		"/lset": "POST",
	}

	a.cache = cache.New(a.config.CacheConfig)

	return a
}

func (a *Arbiter) getIndex(key string, n int) int {
	h := fnv.New32()
	h.Write([]byte(key))
	sum := int(h.Sum32())
	return sum % n
}

func (a *Arbiter) getKey(req *http.Request) (string, error) {
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return "", err
	}
	req.Body.Close()

	var base dto.Base
	err = json.Unmarshal(b, &base)
	if err != nil {
		return "", err
	}

	body := ioutil.NopCloser(bytes.NewReader(b))
	req.Body = body

	if base.Key == nil {
		return "", fmt.Errorf("bad request")
	}

	return *base.Key, nil
}

func (a *Arbiter) getProxy(key string) (*httputil.ReverseProxy, *url.URL) {
	index := a.getIndex(key, len(a.proxies))
	return a.proxies[index], a.urls[index]
}

func (a *Arbiter) handleAuth(req *http.Request) bool {

	auth := strings.SplitN(req.Header.Get("Authorization"), " ", 2)

	if len(auth) != 2 || auth[0] != "Basic" {
		return false
	}

	payload, _ := base64.StdEncoding.DecodeString(auth[1])
	pair := strings.SplitN(string(payload), ":", 2)

	if len(pair) != 2 || !a.config.ACL.Check(pair[0], pair[1]) {
		return false
	}

	return true
}

func (a *Arbiter) handleDelete(res http.ResponseWriter, req *http.Request) {
	key, err := a.getKey(req)
	if err != nil {
		data, err := dto.NewRawError("bad request")
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusBadRequest)
		res.Write(data)
		return
	}

	found := a.cache.Contains(key)
	if !found {
		data, err := dto.NewRawError("not found")
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusNotFound)
		res.Write(data)
		return
	}

	a.serveHTTP(res, req, key)
}

func (a *Arbiter) handleGet(res http.ResponseWriter, req *http.Request) {
	key, err := a.getKey(req)
	if err != nil {
		data, err := dto.NewRawError("bad request")
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusBadRequest)
		res.Write(data)
		return
	}

	found := a.cache.Contains(key)
	if !found {
		data, err := dto.NewRawError("not found")
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusNotFound)
		res.Write(data)
		return
	}

	a.serveHTTP(res, req, key)
}

func (a *Arbiter) handleKeys(res http.ResponseWriter, req *http.Request) {
	keys := a.cache.Keys()

	data, err := dto.NewRawValues(keys)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write(data)
}

func (a *Arbiter) handleSet(res http.ResponseWriter, req *http.Request) {
	key, err := a.getKey(req)
	if err != nil {
		data, err := dto.NewRawError("bad request")
		if err != nil {
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		res.WriteHeader(http.StatusBadRequest)
		res.Write(data)
		return
	}

	a.serveHTTP(res, req, key)
}

func (a *Arbiter) handleRequest(res http.ResponseWriter, req *http.Request) {
	if a.config.EnableAuth {
		if !a.handleAuth(req) {
			res.WriteHeader(http.StatusUnauthorized)
			return
		}
	}

	if req.URL.Path == "/del" {
		a.handleDelete(res, req)
	} else if method, ok := a.getPaths[req.URL.Path]; ok && req.Method == method {
		a.handleGet(res, req)
	} else if method, ok := a.setPaths[req.URL.Path]; ok && req.Method == method {
		a.handleSet(res, req)
	} else if req.URL.Path == "/keys" {
		a.handleKeys(res, req)
	} else {
		res.WriteHeader(http.StatusNotFound)
	}
}

func (a *Arbiter) handleResponse(resp *http.Response) error {
	_, ok := a.setPaths[resp.Request.URL.Path]
	if resp.StatusCode == http.StatusOK && (resp.Request.URL.Path == "/del" || ok) {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		resp.Body.Close()
		body := ioutil.NopCloser(bytes.NewReader(b))
		resp.Body = body

		if resp.Request.URL.Path == "/del" {
			del, err := dto.UnmarshalDel(b)
			if err != nil {
				return err
			}
			a.cache.Del(del.Key)
		} else if _, ok := a.setPaths[resp.Request.URL.Path]; ok {
			value, err := dto.UnmarshalValue(b)
			if err != nil {
				return err
			}
			a.cache.Set(*value.Base.Key, nil, value.Base.Expiration.Duration)
		}
	}

	return nil
}

func (a *Arbiter) serveHTTP(res http.ResponseWriter, req *http.Request, key string) {
	proxy, u := a.getProxy(key)
	a.upgradeRequest(req, u)
	proxy.ServeHTTP(res, req)
}

func (a *Arbiter) upgradeRequest(req *http.Request, u *url.URL) {
	req.URL.Host = u.Host
	req.URL.Scheme = u.Scheme
	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
	req.Host = u.Host
}

func (a *Arbiter) Run() error {
	defer a.cache.Destroy()

	if len(a.config.Nodes) == 0 {
		return fmt.Errorf("nodes list is empty")
	}

	a.urls = make([]*url.URL, 0, len(a.config.Nodes))
	a.proxies = make([]*httputil.ReverseProxy, 0, len(a.config.Nodes))

	for _, node := range a.config.Nodes {
		u, err := url.Parse(node)
		if err != nil {
			return fmt.Errorf("url parse node error: %v", err)
		}

		proxy := httputil.NewSingleHostReverseProxy(u)
		proxy.ModifyResponse = a.handleResponse

		a.proxies = append(a.proxies, proxy)
		a.urls = append(a.urls, u)
	}

	go func() {
		if err := a.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %v\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := a.server.Shutdown(ctx); err != nil {
		return fmt.Errorf("arbiter shutdown error: %v", err)
	}

	return nil
}
