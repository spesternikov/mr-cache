package node

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"mr-cache/internal/pkg/dto"
	"mr-cache/pkg/acl"
	"mr-cache/pkg/cache"

	"github.com/gin-gonic/gin"
)

const (
	DefaultAddr         = "127.0.0.1"
	DefaultPort         = 1234
	DefaultReadTimeout  = time.Minute
	DefaultWriteTimeout = time.Minute
)

type Config struct {
	Addr         string
	ACL          *acl.ACL
	CacheConfig  *cache.Config
	EnableAuth   bool
	Port         int
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
}

type Node struct {
	cache  *cache.Cache
	config Config
	server *http.Server
}

func (c *Config) withDefaults() (config Config) {
	if c != nil {
		config = *c
	}

	if config.Addr == "" {
		config.Addr = DefaultAddr
	}

	if config.Port == 0 {
		config.Port = DefaultPort
	}

	if config.ReadTimeout == 0 {
		config.ReadTimeout = DefaultReadTimeout
	}

	if config.WriteTimeout == 0 {
		config.WriteTimeout = DefaultWriteTimeout
	}

	return config
}

func New(c *Config) *Node {
	node := new(Node)

	node.config = c.withDefaults()

	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()

	if node.config.EnableAuth {
		r.Use(gin.BasicAuth(gin.Accounts(node.config.ACL.GetUsers())))
	}

	r.POST("/del", node.del)
	r.POST("/get", node.get)
	r.POST("/hget", node.hGet)
	r.GET("/keys", node.keys)
	r.POST("/lget", node.lGet)
	r.POST("/set", node.set)
	r.POST("/hset", node.hSet)
	r.POST("/lset", node.lSet)

	node.server = &http.Server{
		Addr:         node.config.Addr + ":" + strconv.Itoa(node.config.Port),
		Handler:      r,
		ReadTimeout:  node.config.ReadTimeout,
		WriteTimeout: node.config.WriteTimeout,
	}

	node.cache = cache.New(node.config.CacheConfig)

	return node
}

// del godoc
// @Summary del
// @Description Delete item from cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param del body dto.Del true "del dto"
// @Success 200 {object} dto.Del
// @Failure 400 {object} dto.Error
// @Failure 404 {object} dto.Error
// @Router /del [post]
func (n *Node) del(c *gin.Context) {
	var del dto.Del
	err := c.ShouldBind(&del)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	found := n.cache.Del(del.Key)
	if !found {
		c.JSON(http.StatusNotFound, dto.NewError("not found"))
		return
	}

	c.JSON(http.StatusOK, del)
}

// get godoc
// @Summary get
// @Description Get string from cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param value body dto.Value true "get dto"
// @Success 200 {object} dto.Value
// @Failure 400 {object} dto.Error
// @Failure 404 {object} dto.Error
// @Router /get [post]
func (n *Node) get(c *gin.Context) {
	var value dto.Value
	err := c.ShouldBind(&value)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if value.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	val, found, err := n.cache.Get(*value.Key)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("cache error: %v", err)))
		return
	}

	fmt.Println("test", value)

	if !found {
		c.JSON(http.StatusNotFound, dto.NewError("not found"))
		return
	}

	value.Value = &val

	c.JSON(http.StatusOK, &value)
}

// get godoc
// @Summary hget
// @Description Get string-hash item from cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param map body dto.Map true "hget dto"
// @Success 200 {object} dto.Value
// @Failure 400 {object} dto.Error
// @Failure 403 {object} dto.Error
// @Failure 404 {object} dto.Error
// @Router /hget [post]
func (n *Node) hGet(c *gin.Context) {
	var m dto.Map
	err := c.ShouldBind(&m)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if m.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	if m.HKey == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("hKey is empty"))
		return
	}

	val, found, err := n.cache.HGet(*m.Key, *m.HKey)
	if err != nil {
		if err == cache.ErrItemNotFound {
			c.JSON(http.StatusForbidden, dto.NewError(fmt.Sprintf("cache error: %v", err)))
		}
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("cache error: %v", err)))
		return
	}

	if !found {
		c.JSON(http.StatusNotFound, dto.NewError("not found"))
		return
	}

	value := dto.Value{
		Base:  m.Base,
		Value: &val,
	}

	c.JSON(http.StatusOK, &value)
}

// keys godoc
// @Summary keys
// @Description Get cache keys
// @Tags cache
// @Produce  json
// @Success 200 {object} dto.Values
// @Router /keys [get]
func (n *Node) keys(c *gin.Context) {
	keys := n.cache.Keys()

	c.JSON(http.StatusOK, dto.NewValues(keys))
}

// get godoc
// @Summary lget
// @Description Get string-list item from cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param list body dto.List true "lget dto"
// @Success 200 {object} dto.Value
// @Failure 400 {object} dto.Error
// @Failure 403 {object} dto.Error
// @Failure 404 {object} dto.Error
// @Router /lget [post]
func (n *Node) lGet(c *gin.Context) {
	var list dto.List
	err := c.ShouldBind(&list)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if list.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	val, found, err := n.cache.LGet(*list.Key, list.Index)
	if err != nil {
		if err == cache.ErrIndexOutRange {
			c.JSON(http.StatusForbidden, dto.NewError(fmt.Sprintf("cache error: %v", err)))
		} else {
			c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("cache error: %v", err)))
		}
		return
	}

	if !found {
		c.JSON(http.StatusNotFound, dto.NewError("not found"))
		return
	}

	value := dto.Value{
		Base:  list.Base,
		Value: &val,
	}

	c.JSON(http.StatusOK, &value)
}

// get godoc
// @Summary set
// @Description Set string to cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param value body dto.Value true "set dto"
// @Success 200 {object} dto.Base
// @Failure 400 {object} dto.Error
// @Router /set [post]
func (n *Node) set(c *gin.Context) {
	var value dto.Value
	err := c.ShouldBind(&value)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if value.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	if value.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("value is empty"))
		return
	}

	n.cache.Set(*value.Key, *value.Value, value.Expiration.Duration)

	c.JSON(http.StatusOK, &value.Base)
}

// get godoc
// @Summary hset
// @Description Set hash to cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param map body dto.Map true "hset dto"
// @Success 200 {object} dto.Base
// @Failure 400 {object} dto.Error
// @Router /hset [post]
func (n *Node) hSet(c *gin.Context) {
	var m dto.Map
	err := c.ShouldBind(&m)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if m.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	if m.Value == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("value is null"))
		return
	}

	n.cache.Set(*m.Key, m.Value, m.Expiration.Duration)

	c.JSON(http.StatusOK, &m.Base)
}

// get godoc
// @Summary lset
// @Description Set list to cache by key
// @Tags cache
// @Accept  json
// @Produce  json
// @Param list body dto.List true "lset dto"
// @Success 200 {object} dto.Base
// @Failure 400 {object} dto.Error
// @Router /lset [post]
func (n *Node) lSet(c *gin.Context) {
	var list dto.List
	err := c.ShouldBind(&list)
	if err != nil {
		c.JSON(http.StatusBadRequest, dto.NewError(fmt.Sprintf("bind error: %v", err)))
		return
	}

	if list.Key == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("key is empty"))
		return
	}

	if list.Value == nil {
		c.JSON(http.StatusBadRequest, dto.NewError("value is null"))
		return
	}

	n.cache.Set(*list.Key, list.Value, list.Expiration.Duration)

	c.JSON(http.StatusOK, &list.Base)
}

func (n *Node) Run() error {
	defer n.cache.Destroy()

	go func() {
		if err := n.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %v\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := n.server.Shutdown(ctx); err != nil {
		return fmt.Errorf("node shutdown error: %v", err)
	}

	return nil
}
