package dto

import (
	"encoding/json"
	"fmt"
)

type Values struct {
	Values []string `json:"values" example:"key1,key2"`
}

func NewValues(values []string) *Values {
	return &Values{
		Values: values,
	}
}

func NewRawValues(values []string) ([]byte, error) {
	e := NewValues(values)

	data, err := json.Marshal(e)
	if err != nil {
		return nil, fmt.Errorf("json marshal values error: %v", err)
	}

	return data, err
}
