package dto

type List struct {
	Base
	Index      int           `json:"index" example:"1"`
	Value      []string      `json:"value,omitempty" example:"value1,value2"`
}
