package dto

import (
	"encoding/json"
	"fmt"
)

type Value struct {
	Base
	Value *string `json:"value,omitempty" example:"value"`
}

func UnmarshalValue(data []byte) (*Value, error) {
	var v Value

	err := json.Unmarshal(data, &v)
	if err != nil {
		return nil, fmt.Errorf("json unmarshal value error: %v", err)
	}

	return &v, nil
}
