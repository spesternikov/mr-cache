package dto

type Map struct {
	Base
	HKey  *string           `json:"hKey" example:"key"`
	Value map[string]string `json:"value,omitempty"`
}
