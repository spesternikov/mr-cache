package dto

import (
	"encoding/json"
	"fmt"
)

type Error struct {
	Message string `json:"message" example:"message"`
}

func NewError(message string) *Error {
	return &Error{
		Message: message,
	}
}

func NewRawError(message string) ([]byte, error) {
	e := NewError(message)

	data, err := json.Marshal(e)
	if err != nil {
		return nil, fmt.Errorf("json marshal error error: %v", err)
	}

	return data, err
}
