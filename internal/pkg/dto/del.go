package dto

import (
	"encoding/json"
	"fmt"
)

type Del struct {
	Key string `json:"key" example:"key"`
}

func UnmarshalDel(data []byte) (*Del, error) {
	var d Del

	err := json.Unmarshal(data, &d)
	if err != nil {
		return nil, fmt.Errorf("json unmarshal del error: %v", err)
	}

	return &d, nil
}