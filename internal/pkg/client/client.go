package client

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"mr-cache/internal/pkg/dto"

	"github.com/kballard/go-shellquote"
)

const (
	DefaultTimeout = time.Minute
)

var (
	ErrHelpCommand    = errors.New("help")
	ErrInvalidCommand = errors.New("invalid command")
)

type Config struct {
	Url      string
	Password string
	Timeout  time.Duration
	User     string
}

type Client struct {
	config Config
	client *http.Client
}

type command struct {
	Expiration time.Duration
	Key        string
	Operation  string
	Value      interface{}
}

func (c *Config) withDefaults() (config Config) {
	if c != nil {
		config = *c
	}

	if config.Timeout == 0 {
		config.Timeout = DefaultTimeout
	}

	return config
}

func New(c *Config) *Client {
	client := new(Client)

	client.config = c.withDefaults()

	client.client = &http.Client{
		Timeout: client.config.Timeout,
	}

	return client
}

func (c *Client) basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func (c *Client) unmarshalList(value string) ([]string, error) {
	var res []string
	err := json.Unmarshal([]byte(value), &res)
	if err != nil {
		return nil, ErrInvalidCommand
	}

	return res, nil
}

func (c *Client) unmarshalMap(value string) (map[string]string, error) {
	var res map[string]string
	err := json.Unmarshal([]byte(value), &res)
	if err != nil {
		return nil, ErrInvalidCommand
	}

	return res, nil
}

func (c *Client) parseCommand(rawCommand string) (*command, error) {
	parts, err := shellquote.Split(rawCommand)
	if err != nil {
		return nil, ErrInvalidCommand
	}

	if len(parts) == 0 {
		return nil, ErrInvalidCommand
	}

	operation := parts[0]

	if len(parts) == 1 {
		switch operation {
		case "help":
			return nil, ErrHelpCommand
		case "keys":
			return &command{
				Operation: operation,
			}, nil
		}
	}

	if len(parts) < 2 {
		return nil, ErrInvalidCommand
	}
	key := parts[1]
	switch operation {
	case "del":
		return &command{
			Key:       key,
			Operation: operation,
		}, nil
	default:
		return c.parseManageCommand(operation, key, parts)
	}
}

func (c *Client) parseGetCommand(operation string, key string, parts []string) (*command, error) {
	if len(parts) != 3 {
		return nil, ErrInvalidCommand
	}

	switch operation {
	case "hget":
		return &command{
			Key:       key,
			Operation: operation,
			Value:     parts[2],
		}, nil
	case "lget":
		index, err := strconv.Atoi(parts[2])
		if err != nil {
			return nil, ErrInvalidCommand
		}
		return &command{
			Key:       key,
			Operation: operation,
			Value:     index,
		}, nil
	default:
		return nil, ErrInvalidCommand
	}
}

func (c *Client) parseManageCommand(operation string, key string, parts []string) (*command, error) {
	if len(parts) < 2 {
		return nil, ErrInvalidCommand
	}
	switch operation {
	case "get":
		return &command{
			Key:       key,
			Operation: operation,
		}, nil
	case "hget", "lget":
		return c.parseGetCommand(operation, key, parts)
	case "set", "hset", "lset":
		return c.parseSetCommand(operation, key, parts)
	default:
		return nil, ErrInvalidCommand
	}
}

func (c *Client) parseSetCommand(operation string, key string, parts []string) (*command, error) {
	if len(parts) < 3 {
		return nil, ErrInvalidCommand
	}

	var expiration time.Duration
	if len(parts) == 4 {
		var err error
		expiration, err = time.ParseDuration(parts[3])
		if err != nil {
			return nil, ErrInvalidCommand
		}
	}

	switch operation {
	case "set":
		return &command{
			Key:        key,
			Operation:  operation,
			Value:      parts[2],
			Expiration: expiration,
		}, nil
	case "hset":
		value, err := c.unmarshalMap(parts[2])
		if err != nil {
			return nil, ErrInvalidCommand
		}
		return &command{
			Key:        key,
			Operation:  operation,
			Value:      value,
			Expiration: expiration,
		}, nil
	case "lset":
		value, err := c.unmarshalList(parts[2])
		if err != nil {
			return nil, ErrInvalidCommand
		}
		return &command{
			Key:        key,
			Operation:  operation,
			Value:      value,
			Expiration: expiration,
		}, nil

	default:
		return nil, ErrInvalidCommand
	}
}

func (c *Client) printHelp() {
	help := "Available commands:\n" +
		"get key - get string from cache\n" +
		"set key value [expiration] - set string to cache by key\n" +
		"hget key hkey - get hash string by hkey\n" +
		"hset key hash [expiration] - set hash to cache by key\n" +
		"lget key index - get string from list by index\n" +
		"lset key list [expiration] - set list to cache by key\n " +
		"del key - delete key from cache\n" +
		"keys - get cache keys\n" +
		"help - show help\n\n" +
		"Description:\n" +
		"value - string\n" +
		"hkey - string\n" +
		"index - string\n" +
		"hash - json object\n" +
		"list - json string array\n"

	fmt.Printf(help)
}

func (c *Client) Do(ctx context.Context, rawCommand string) error {
	com, err := c.parseCommand(rawCommand)
	if err != nil {
		if err == ErrHelpCommand {
			c.printHelp()
			return nil
		}
		return err
	}

	body := c.createBody(com)

	method := "POST"
	if com.Operation == "keys" {
		method = "GET"
	}

	b, err := json.Marshal(body)
	if err != nil {
		return fmt.Errorf("json marshal body error: %v", err)
	}

	req, err := http.NewRequest(method, c.config.Url+"/"+com.Operation, bytes.NewBuffer(b))
	if err != nil {
		return fmt.Errorf("create request error: %v", err)
	}

	req.Header.Add("Authorization", "Basic "+c.basicAuth(c.config.User, c.config.Password))
	req.Header.Add("Content-Type", "application/json")

	req.WithContext(ctx)

	res, err := c.client.Do(req)
	if err != nil {
		return fmt.Errorf("do request error: %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusOK {
		switch com.Operation {
		case "del", "hset", "lset", "set":
			fmt.Printf("OK\n")
		case "get", "hget", "lget":
			var value dto.Value
			err := json.NewDecoder(res.Body).Decode(&value)
			if err != nil {
				return fmt.Errorf("json marshal response body error: %v", err)
			}
			fmt.Printf("%s\n", *value.Value)
		case "keys":
			var values dto.Values
			err := json.NewDecoder(res.Body).Decode(&values)
			if err != nil {
				return fmt.Errorf("json marshal response body error: %v", err)
			}
			fmt.Printf("[%s]\n", strings.Join(values.Values, ","))
		}
	} else if res.StatusCode == http.StatusNotFound {
		fmt.Printf("(nil)\n")
	} else if res.StatusCode == http.StatusBadRequest {
		fmt.Printf("bad request\n")
	} else if res.StatusCode == http.StatusUnauthorized {
		fmt.Printf("unauthorized\n")
	} else if res.StatusCode == http.StatusForbidden && com.Operation == "hget" {
		fmt.Printf("item not found\n")
	} else if res.StatusCode == http.StatusForbidden && com.Operation == "lget" {
		fmt.Printf("index out of range\n")
	} else {
		var e dto.Error
		err := json.NewDecoder(res.Body).Decode(&e)
		if err != nil {
			return fmt.Errorf("json marshal response body error: %v", err)
		}
		fmt.Printf("response status code: %d; error: %s\n", res.StatusCode, e.Message)
	}

	return nil
}

func (c *Client) createBody(com *command) interface{} {
	switch com.Operation {
	case "del":
		return &dto.Value{
			Base: dto.Base{
				Key: &com.Key,
			},
		}
	case "get":
		return &dto.Value{
			Base: dto.Base{
				Key: &com.Key,
			},
		}
	case "hget":
		val := com.Value.(string)
		return &dto.Map{
			Base: dto.Base{
				Key: &com.Key,
			},
			HKey: &val,
		}
	case "lget":
		return &dto.List{
			Base: dto.Base{
				Key: &com.Key,
			},
			Index: com.Value.(int),
		}
	case "set":
		value := com.Value.(string)
		return &dto.Value{
			Base: dto.Base{
				Expiration: dto.Duration{Duration: com.Expiration},
				Key:        &com.Key,
			},
			Value: &value,
		}
	case "hset":
		return &dto.Map{
			Base: dto.Base{
				Expiration: dto.Duration{Duration: com.Expiration},
				Key:        &com.Key,
			},
			Value: com.Value.(map[string]string),
		}
	case "lset":
		return &dto.List{
			Base: dto.Base{
				Expiration: dto.Duration{Duration: com.Expiration},
				Key:        &com.Key,
			},
			Value: com.Value.([]string),
		}
	default:
		return nil
	}
}
